% This is an example of how to modify the style of your
% thesis.  Check the guidelines for your school/university
% on the required format for a PhD thesis.

% First identify the class:

\NeedsTeXFormat{LaTeX2e}
\ProvidesClass{mytemplate}

\DeclareOption*{\PassOptionsToClass{\CurrentOption}{book}}
%\ExecuteOptions{12pt,a4paper,openright,twoside}

\ProcessOptions

\LoadClass[12pt,a4paper,openright,twoside]{book}

% Set paper size and margins
%\RequirePackage{geometry}



% Make the captions in your floats of the form:
% Fig. 1
% instead of:
% Figure 1

%\renewcommand{\figurename}{Fig.}
%\renewcommand{\tablename}{Tab.}

% Redefining the \section command:


% Redefining the chapter heading styles

% Numbered chapter heading style:

\renewcommand{\@makechapterhead}[1]{%
  \vspace*{50\p@}%
  {
  \parindent \z@ \raggedright \normalfont
    %\hrule                                           % horizontal line
    \vspace{5pt}%                                    % add some vertical space
    \ifnum \c@secnumdepth >\m@ne
    	\begin{flushright}
        	\Large\scshape \bfseries \@chapapp\space \thechapter    % Chapter followed by number
    	\end{flushright}
        \par\nobreak
        \vskip 5\p@
    \fi
    \hrule                                           % horizontal rule
    \interlinepenalty\@M
    \begin{flushright}

    \fontsize{30}{30} \selectfont \bfseries \scshape #1\par                            % chapter title

    \end{flushright}
%    \vspace{15pt}%                                    % add some vertical space
    \nobreak
    \vskip 35\p@
  }}

% Unnumbered chapter heading style:

\renewcommand{\@makeschapterhead}[1]{%
  \vspace*{50\p@}%
  {\parindent \z@ \raggedright
    \normalfont

    %\hrule                                           % horizontal line
    \vspace{5pt}%                                    % add some vertical space
    \interlinepenalty\@M
    \begin{flushright}

    \fontsize{30}{35} \selectfont \bfseries \scshape #1\par                            % chapter title

    \end{flushright}
%    \vspace{5pt}%                                    % add some vertical space
%    \hrule                                           % horizontal line
    \nobreak
    \vskip 35\p@
  }}

% end of file:
\endinput
